// ds3231.h.h

#ifndef _DS3231.H_h
#define _DS3231.H_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
typedef struct ext_rtc_calendar_time {
	uint8_t id;
	/** Second value */
	uint8_t  second;
	/** Minute value */
	uint8_t  minute;
	/** Hour value */
	uint8_t  hour;
	/** PM/AM value, \c true for PM, or \c false for AM */
	bool     pm;
	/** Day value, where day 1 is the first day of the month */
	uint8_t  day;
	/** Month value, where month 1 is January */
	uint8_t  month;
	/** Year value */
	uint16_t year;
} ExtRTC_time;

#endif

