#include "ds3231.h"
#include <Wire.h>
#include "RTClib.h"
#include <EEPROM.h>
#define INPUT_SIZE 40
#define MAX_NO_RECORDS	10

const byte interruptPin = 2;
const byte relayPin = 3;
char inputString[INPUT_SIZE];         // a String to hold incoming data
bool stringComplete = false, stringTokenStatus = false;  // whether the string is complete

//
bool alarmRecordPointerStatus = false;
uint8_t alarmRecordPointer = 0;
bool rtcReadFlag = false;
uint8_t idPointerGlobel = 0;

typedef enum
{
	APP_TASK_ACQUIRE_ID,
	APP_TASK_CAMPARE_TIME,
	APP_TASK_GENARATE_SIGNAL
}APP_TASKS_t;
APP_TASKS_t	APP_TASKS;

#define ledPin 13
int timer1_counter;

uint16_t currentAddress = 0, pastAddress = 0;
uint8_t	eepromStructSize = 0;
ExtRTC_time dayTime_serial, dayTime_eeprom[MAX_NO_RECORDS], dayTimeCurrentGlobel;
 
uint8_t stringTokeniser(void);
uint8_t eepromSave(void);
void printAllRecords(void);

RTC_DS3231 rtc;

void setup() {
	//
	pinMode(ledPin, OUTPUT);
	pinMode(relayPin, OUTPUT);
	
	// initialize timer1
	noInterrupts();           // disable all interrupts
	TCCR1A = 0;
	TCCR1B = 0;

	//timer1_counter = 34286;   // preload timer 65536-16MHz/256/2Hz
	timer1_counter = 1714;		//every sec
	//timer1_counter = 0;
	
	TCNT1 = timer1_counter;   // preload timer
	TCCR1B |= (1 << CS12);    // 256 prescaler
	TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
	interrupts();             // enable all interrupts
	//	
	eepromStructSize = sizeof(ExtRTC_time);

	// initialize serial:
	Serial.begin(57600);
	Serial.println("welcome");
	
	delay(2000); // wait for console opening

	if (! rtc.begin()) {
		Serial.println("Couldn't find RTC");
		while (1);
	}

	if (rtc.lostPower()) {
		Serial.println("RTC lost power, lets set the time!");
		// following line sets the RTC to the date & time this sketch was compiled
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
		// This line sets the RTC with an explicit date & time, for example to set
		// January 21, 2014 at 3am you would call:
		// rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
	}
	//delay(1000);
	
	compareAndSendPresentId();
	eepromRead(idPointerGlobel, &dayTimeCurrentGlobel);
	printtemp();
	
}

void loop() {
	
	if(stringComplete == true)
	{
		if (stringTokenStatus == true)
		{
			eepromSave();
			stringTokenStatus = false;
		}
	
		stringComplete = false;
	}
	stringTokeniser();
	if (rtcReadFlag == true)
	{
		
		compareTimeAndTiggerAlarm();
	}
	
}

void compareTimeAndTiggerAlarm()
{
	DateTime now = rtc.now();
	
	if (now.year() == dayTimeCurrentGlobel.year)
	{
		if (now.month() == dayTimeCurrentGlobel.month)
		{
			if (now.day() == dayTimeCurrentGlobel.day)
			{
				if (now.hour() == dayTimeCurrentGlobel.hour)
				{
					if (now.minute() == dayTimeCurrentGlobel.minute)
					{
						alarmTrigered();
					}
				}
			}
		}
	}
	Serial.println();
	Serial.print(now.year(), DEC);
	Serial.print('/');
	Serial.print(now.month(), DEC);
	Serial.print('/');
	Serial.print(now.day(), DEC);
	Serial.print(':');
	Serial.print(now.hour(), DEC);
	Serial.print(':');
	Serial.print(now.minute(), DEC);
	Serial.print(':');
	Serial.print(now.second(), DEC);
	Serial.println();
	
	rtcReadFlag = false;
	
}
uint8_t compareAndSendPresentId()
{
	DateTime now = rtc.now();
	uint8_t i;
	ExtRTC_time	dayTime_eeprom_l;
	uint16_t currentAddress = 0;
	for (i = 1; i<=MAX_NO_RECORDS; i++)
	{
		currentAddress = (i) * (eepromStructSize);
		EEPROM.get(currentAddress, dayTime_eeprom_l);
		delay(200);
		if (dayTime_eeprom_l.id > 0 && dayTime_eeprom_l.id < 255)
		{
			if (now.year() > dayTime_eeprom_l.year)
			{
				Serial.println("old record by year");
			}
			else if(now.year() < dayTime_eeprom_l.year)
			{
				idPointerGlobel = i;
				break;				
			}
			else if(now.year() == dayTime_eeprom_l.year)
			{
				if (now.month() > dayTime_eeprom_l.month)
				{

					Serial.println("old record by month");
				}
				else if(now.month() < dayTime_eeprom_l.month)
				{
					idPointerGlobel = i;
					break;
				}
				else if(now.month() == dayTime_eeprom_l.month)
				{
					
					if (now.day() > dayTime_eeprom_l.day)
					{
						Serial.println("old record by day");
					}
					else if(now.day() < dayTime_eeprom_l.day)
					{
						idPointerGlobel = i;
						break;	
					}
					else if(now.day() == dayTime_eeprom_l.day)
					{
						if (now.hour() > dayTime_eeprom_l.hour)
						{

							Serial.println("old record by hour");
						}
						else if(now.hour() < dayTime_eeprom_l.hour)
						{
							idPointerGlobel = i;
							break;
						}
						else if(now.hour() == dayTime_eeprom_l.hour)
						{
							if (now.minute() > dayTime_eeprom_l.minute)
							{
								Serial.println("old record by minute");
							}
							else if(now.minute() < dayTime_eeprom_l.minute)
							{
								idPointerGlobel = i;
								break;
								
							}
							else if(now.minute() == dayTime_eeprom_l.minute)
							{
									Serial.println("given record is same as running time");					
							}						
						}
					}
				}
			}
		} 
		else
		{
			Serial.println("no records please enter some records");
		}
		
	}
}

void printtemp()
{
	Serial.println();
	Serial.print("current record is:");
	Serial.print(dayTimeCurrentGlobel.id);Serial.print(",date=");
	Serial.print(dayTimeCurrentGlobel.day);Serial.print(":");
	Serial.print(dayTimeCurrentGlobel.month);Serial.print(":");
	Serial.print(dayTimeCurrentGlobel.year);Serial.print(",time=");
	Serial.print(dayTimeCurrentGlobel.hour);Serial.print(":");
	Serial.println(dayTimeCurrentGlobel.minute);
	Serial.println("success");
}

uint8_t eepromRead(uint8_t id, ExtRTC_time *dayTime_eeprom_l)
{
	//ExtRTC_time	dayTime_eeprom_l;
	uint16_t currentAddress = 0;
	currentAddress = (id) * (eepromStructSize);
	EEPROM.get(currentAddress, *dayTime_eeprom_l);
	delay(200);
	/*Serial.print("data stored at address:");
	Serial.print(currentAddress);
	Serial.print("and stored data is id=");
	Serial.print(dayTime_eeprom_l.id);Serial.print(",date=");
	Serial.print(dayTime_eeprom_l.day);Serial.print(":");
	Serial.print(dayTime_eeprom_l.month);Serial.print(":");
	Serial.print(dayTime_eeprom_l.year);Serial.print(",time=");
	Serial.print(dayTime_eeprom_l.hour);Serial.print(":");
	Serial.println(dayTime_eeprom_l.minute);
	Serial.println("success");*/
	
	
}

uint8_t eepromSave(void)
{
	ExtRTC_time	dayTime_eeprom_l;
	uint16_t currentAddress = 0;
	
	if (dayTime_serial.id >0 && dayTime_serial.id <= MAX_NO_RECORDS)
	{
		//pastAddress = currentAddress;
		currentAddress = (dayTime_serial.id) * (eepromStructSize);
		uint8_t buffer[50];	
		EEPROM.put(currentAddress, dayTime_serial);
		delay(200);
		EEPROM.get(currentAddress, dayTime_eeprom_l);
		delay(200);
		
		Serial.println();
		Serial.print("data stored at address:");
		Serial.print(currentAddress);
		Serial.print("and stored data is id=");
		Serial.print(dayTime_eeprom_l.id);Serial.print(",date=");
		Serial.print(dayTime_eeprom_l.day);Serial.print(":");
		Serial.print(dayTime_eeprom_l.month);Serial.print(":");
		Serial.print(dayTime_eeprom_l.year);Serial.print(",time=");
		Serial.print(dayTime_eeprom_l.hour);Serial.print(":");
		Serial.println(dayTime_eeprom_l.minute);
		Serial.println("success");
	}
	else
	{
		Serial.println("input cmd is wrong/ID is out of range");
	}
}
uint8_t stringTokeniser()
{
	while (Serial.available()) {
	byte size = Serial.readBytesUntil('\n', inputString, INPUT_SIZE);
	inputString[size] = 0;
	//Serial.println(inputString);
	if (!(strcmp(inputString, "printall")))
	{
		Serial.println("printall");
		printAllRecords();
		stringComplete = true;
		return 1;
	}
	else if (!(strcmp(inputString, "clearall")))
	{
		Serial.println("clearall");
		for (int i = 0 ; i < 100 ; i++) {
			EEPROM.write(i, 0);
		}
		stringComplete = true;
		return 1;
	}
	else if (!(strcmp(inputString, "time?")))
	{
		Serial.println("time?");
		
		stringComplete = true;
		return 1;
	}
	else if (!(strcmp(inputString, "record")))
	{
		Serial.println("record");
		printtemp();
		stringComplete = true;
		return 1;
	}
	else if (!(strcmp(inputString, "enter")))
	{
		Serial.println("enter pressed");
		idPointerGlobel = 0;
		compareAndSendPresentId();
		eepromRead(idPointerGlobel, &dayTimeCurrentGlobel);
		printtemp();
		stringComplete = true;
		return 1;
	}
	else
	{
		char *saveptr;
		char *first, *seconde, *third;
		first = strtok_r(inputString, ",", &saveptr);
		seconde = strtok_r(NULL, ",", &saveptr);
		third = strtok_r(NULL, ",", &saveptr);
		//Serial.println(first);
		*saveptr = NULL;
		strtok_r(first, "=", &saveptr);
		dayTime_serial.id =(uint8_t) atoi(strtok_r(NULL, "=", &saveptr));
	
		*saveptr = NULL;
		strtok_r(seconde, "=", &saveptr);
		dayTime_serial.day = (uint8_t) atoi(strtok_r(NULL, ":", &saveptr));
		dayTime_serial.month = (uint8_t) atoi(strtok_r(NULL, ":", &saveptr));
		dayTime_serial.year = (uint16_t) atoi(strtok_r(NULL, "", &saveptr));

		*saveptr = NULL;
		strtok_r(third, "=", &saveptr);
		dayTime_serial.hour = (uint8_t) atoi(strtok_r(NULL, ":", &saveptr));
		dayTime_serial.minute = (uint8_t) atoi(strtok_r(NULL, "", &saveptr));
		if (dayTime_serial.id < 10 && dayTime_serial.month < 13 && dayTime_serial.hour < 25)
		{
			stringComplete = true;
			stringTokenStatus = true;
		}
		/*Serial.println(dayTime.id);  
		Serial.println(dayTime.day);
		Serial.println(dayTime.month);
		Serial.println(dayTime.year);
		Serial.println(dayTime.hour);
		Serial.println(dayTime.minute);
		Serial.println();*/
		}
	}
}
void printAllRecords()
{
	ExtRTC_time dayTime_eeprom_l[MAX_NO_RECORDS];
	currentAddress = 0;
	for (int i = 0; i < MAX_NO_RECORDS; i++)
	{
		
		currentAddress = currentAddress + eepromStructSize;
		EEPROM.get(currentAddress, dayTime_eeprom_l[i]);
		delay(200);
		if (dayTime_eeprom_l[i].id > MAX_NO_RECORDS || dayTime_eeprom_l[i].id ==0)
		{
			Serial.println("no records");
			break;
		}
		Serial.println();
		Serial.print("data stored at address:");
		Serial.print(currentAddress);
		Serial.print("and stored data is id=");
		Serial.print(dayTime_eeprom_l[i].id);Serial.print(",date=");
		Serial.print(dayTime_eeprom_l[i].day);Serial.print(":");
		Serial.print(dayTime_eeprom_l[i].month);Serial.print(":");
		Serial.print(dayTime_eeprom_l[i].year);Serial.print(",time=");
		Serial.print(dayTime_eeprom_l[i].hour);Serial.print(":");
		Serial.println(dayTime_eeprom_l[i].minute);
		Serial.println("success");

	}
	
}
void alarmTrigered()
{
	Serial.println("event triggered");
	digitalWrite(relayPin, 1);
	delay(5000);
	digitalWrite(relayPin, 0);
	
	/*idPointerGlobel++;
	if (idPointerGlobel <= MAX_NO_RECORDS)
	{
		eepromRead(idPointerGlobel, &dayTimeCurrentGlobel);
		printtemp();
	}	*/
	delay(61000);
		
	idPointerGlobel = 0;
	compareAndSendPresentId();
	eepromRead(idPointerGlobel, &dayTimeCurrentGlobel);
	printtemp();
}
ISR(TIMER1_OVF_vect)        // interrupt service routine
{
	TCNT1 = timer1_counter;   // preload timer
	digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
	rtcReadFlag = true;
}
//sample commands
//id=1,date=11:11:2018,time=12:00
//printall
//clearall
//record
//enter
